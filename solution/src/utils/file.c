#include "../../include/utils/file.h"

enum operation_status open_file(FILE **in, const char *const filename, const char *const mode)
{
    *in = fopen(filename, mode);
    if (*in == NULL)
    {
        return OPEN_ERROR;
    }
    else
        return OPEN_OK;
}

enum operation_status close_file(FILE *in)
{
    int64_t res = fclose(in);
    if (res != 0)
    {
        return CLOSE_ERROR;
    }
    else
        return CLOSE_OK;
}
