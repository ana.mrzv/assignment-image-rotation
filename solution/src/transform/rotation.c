#include "../../include/transform/rotation.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

enum operation_status get_rotated_image(struct image *img)
{
    struct image rotated_img = image_create(img->height, img->width);
    if (!rotated_img.data)
        return ROTATE_IMG_DATA_ALLOCATION_ERROR; //не удалось выделить память
    
        struct pixel read_pixel;
        for (uint64_t i = 0; i < img->height; i++)
        {
            for (uint64_t j = 0; j < img->width; j++)
            {
                read_pixel = img->data[(img->width) * i + j];
                rotated_img.data[(rotated_img.width) * j + img->height - 1 - i] = read_pixel;
            }
        }
        image_free(img);
        *img = rotated_img;
        return ROTATE_OK;
}
