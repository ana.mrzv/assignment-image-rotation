#include "../include/image/image.h"
#include "../include/format/bmp.h"
#include "../include/transform/rotation.h"
#include "../include/utils/file.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    if (argc != 3)
    {
        printf("Недостаточно аргументов\n");
        return 0;
    }

    FILE *in = NULL;
    FILE *out = NULL;
    struct image img = {0}; //для ответа

    switch (open_file(&in, argv[1], "rb"))
    {
    case OPEN_ERROR:
        printf("Ошибка при открытии файла\n");
        return 1;
    case OPEN_OK:
        printf("Файл открылся\n");
        break;
    }

    switch (from_bmp(in, &img))
    {
    case READ_IMG_DATA_ALLOCATION_ERROR:
        printf("Не удалось выделить память картинке\n");
        return 1;
        break;
    case READ_HEADER_ERROR:
        printf("Ошибка при чтении заголовка\n");
        return 1;
        break;
    case READ_INVALID_HEADER:
        printf("В заголовке нет bOffBits\n");
        return 1;
        break;
    case READ_PIXEL_NULL:
        printf("Ошибка при чтении пикселя\n");
        return 1;
        break;
    case READ_PADDING_ERROR:
        printf("Ошибка при чтении паддинга\n");
        return 1;
        break;
    case READ_OFFSET_ERROR:
        printf("Ошибка при чтении оффсета\n");
        return 1;
        break;
    case READ_NULL:
        printf("Ошибка при чтении: null\n");
        return 1;
        break;
    case READ_OK:
        printf("Картинка прочиталась\n");
        break;
    }

    switch (close_file(in))
    {
    case CLOSE_ERROR:
        printf("Ошибка при закрытии файла\n");
        return 1;
        break;
    case CLOSE_OK:
        printf("Файл закрылся\n");
        break;
    }

    switch (get_rotated_image(&img))
    {
    case ROTATE_IMG_DATA_ALLOCATION_ERROR:
        printf("Не удалось выделить память на новую картинку\n");
        return 1;
        break;
    case ROTATE_OK:
        printf("Картинка перевернута\n");
        break;
    }

    switch (open_file(&out, argv[2], "wb"))
    {
    case OPEN_ERROR:
        printf("Ошибка при открытии файла записи\n");
        return 1;
        break;
    case OPEN_OK:
        printf("Файл записи открылся\n");
        break;
    }

    switch (to_bmp(out, &img))
    {
    case WRITE_NULL:
        printf("Указатель на новую картинку null\n");
        return 1;
        break;
    case WRITE_HEADER_ERROR:
        printf("Ошибка при записи заголовков\n");
        return 1;
        break;
    case WRITE_PIXEL_ERROR:
        printf("Ошибка при записи пикселя\n");
        return 1;
        break;
    case WRITE_PADDING_ERROR:
        printf("Ошибка при записи в padding\n");
        return 1;
        break;
    case WRITE_OK:
        printf("Картинка прочиталась\n");
        break;
    }

    switch (close_file(out))
    {
    case CLOSE_ERROR:
        printf("Ошибка при закрытии получившегося файла\n");
        return 1;
        break;
    case CLOSE_OK:
        printf("Итоговый закрылся\n");
        break;
    }

    image_free(&img);

    printf("Готово!\n");

    return 0;
}
