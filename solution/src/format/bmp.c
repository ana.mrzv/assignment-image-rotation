#include "../../include/format/bmp.h"
#include <stdlib.h>
#include <stdio.h>

enum operation_status from_bmp(FILE *in, struct image *img)
{
    if (!in)
        return READ_NULL;
    else
    {
        struct bmp_header header = {0};
        enum operation_status status = read_header(in, &header);
        if (status == READ_OK)
        {
            //читаем общую инфу про картинку
            *img = image_create(header.biWidth, header.biHeight);
            if (!img->data)
                return READ_IMG_DATA_ALLOCATION_ERROR; //не удалось выделить память
            else
            {
                if (fseek(in, header.bOffBits, SEEK_SET) != 0)
                    return READ_OFFSET_ERROR;
                //читаем конкретно пиксели картинки
                return read_pixels(in, &header, img);
            }
        }
        else
            return status;
    }
}

enum operation_status read_header(FILE *in, struct bmp_header *header)
{
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_HEADER_ERROR;
    else
        return READ_OK;
}

enum operation_status read_pixels(FILE *in, struct bmp_header *header, struct image* const img)
{
    if (fseek(in, header->bOffBits, SEEK_SET) != 0)
        return READ_INVALID_HEADER;
    else
    {
        //понять как читать пиксели, с padding или нет
        uint32_t width = img->width;
        size_t padding = get_padding(width);
        struct pixel read_pixel;
        for (uint64_t i = 0; i < img->height; i++) //строчка
        {
            if (fread(img->data+(img->width)*i, sizeof(struct pixel), img->width, in) != width)
                return READ_PIXEL_NULL;            
            if (fseek(in, padding, SEEK_CUR) != 0) //проверка что есть высчитанный padding
                return READ_PADDING_ERROR;
        }
        return READ_OK;
    }
}

size_t get_padding(uint32_t width)
{
    if ((width * 3) % 4 != 0)
    {
        return 4 - (width * 3) % 4;
    }
    return 0;
}

enum operation_status to_bmp(FILE *out, struct image* const img)
{
    if (!img)
    {
        return WRITE_NULL;
    }
    else
    {
        if (write_header(out, img->width, img->height) == WRITE_OK)
        {
            return write_pixels(out, img);
        }
        return WRITE_HEADER_ERROR;
    }
}

enum operation_status write_header(FILE *out, uint32_t width, uint32_t height)
{
    struct bmp_header header = {0};
    size_t padding = get_padding(width);

    header.bfType = 0x4D42;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (width * 3 + padding) * height;
    header.bfileSize = (width * 3 + padding) * height + sizeof(struct bmp_header);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_HEADER_ERROR; //записали заголовок
    return WRITE_OK;
}

enum operation_status write_pixels(FILE *out, struct image* const img)
{
    size_t padding = get_padding(img->width);
    struct pixel write_pixel;
    for (uint64_t i = 0; i < img->height; i++) //строчка
    {
        if (fwrite(img->data + (img->width) * i, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_PIXEL_ERROR;
        if (fseek(out, padding, SEEK_CUR) != 0)
            return WRITE_PADDING_ERROR;
    }
    return WRITE_OK;
}
