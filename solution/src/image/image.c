#include "../../include/image/image.h"
#include <stdlib.h>

struct image image_create(const uint32_t width, const uint32_t height) {
    struct image new_image = {0};
    new_image.width=width;
    new_image.height=height;
    new_image.data=malloc(sizeof(struct pixel)*width*height);
    return new_image;
}

void image_free(struct image* image) {
    free(image->data);
}