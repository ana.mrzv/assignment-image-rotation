#ifndef ROTATION_H
#define ROTATION_H

#include "../image/image.h"
#include "../utils/file.h"

enum operation_status get_rotated_image(struct image *img);

#endif
