#ifndef IMAGE_H
#define IMAGE_H

#include  <inttypes.h>

#pragma pack(push, 1)
struct pixel {
  uint8_t b, g, r; 
};
#pragma pack(pop)

struct image {
  uint32_t width, height;
  struct pixel* data;
};

struct image image_create(const uint32_t width, const uint32_t height);
void image_free(struct image* image);

#endif


