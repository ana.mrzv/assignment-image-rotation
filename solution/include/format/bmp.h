#ifndef BMP_H
#define BMP_H

#include "../image/image.h"
#include "../utils/file.h"
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

#pragma pack(pop)

enum operation_status from_bmp(FILE *in, struct image *img);
enum operation_status read_header(FILE *in, struct bmp_header *header);
enum operation_status read_pixels(FILE *in, struct bmp_header *header, struct image* const img);

size_t get_padding(uint32_t width);
enum operation_status to_bmp(FILE *out, struct image* const img);
enum operation_status write_header(FILE *out, uint32_t width, uint32_t height);
enum operation_status write_pixels(FILE *out, struct image* const img);

#endif

