#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <inttypes.h>

enum operation_status  {
  OPEN_OK = 0,
  OPEN_ERROR,
  CLOSE_OK,
  CLOSE_ERROR,
  
  ROTATE_OK,
  ROTATE_IMG_DATA_ALLOCATION_ERROR,

  READ_OK,
  READ_INVALID_HEADER,
  READ_NULL,
  READ_PIXEL_NULL,
  READ_PADDING_ERROR,
  READ_HEADER_ERROR,
  READ_IMG_DATA_ALLOCATION_ERROR,
  READ_OFFSET_ERROR,

  WRITE_OK,
  WRITE_NULL,
  WRITE_HEADER_ERROR,
  WRITE_PIXEL_ERROR,
  WRITE_PADDING_ERROR
};

enum operation_status open_file(FILE **in, const char *const filename, const char *const mode);
enum operation_status close_file(FILE *in);

#endif
